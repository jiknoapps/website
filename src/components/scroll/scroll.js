import { scrollTo } from '../../services/store';

export default (number) => {
	scrollTo.update((arr) => {
		arr.push(number);
		return arr;
	});
};
