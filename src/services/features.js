export default [
	{
		title: '100% offline support',
		description:
			'On our web apps, you can browse, add comments to the blog posts, close the tab, and restart your computer, all with no internet connection. All your work will be saved.',
		image: 'website-offline.svg',
	},
	{
		title: 'Client side routing',
		description:
			'When you navigate between pages on one of the applictions that we build, the load is instantaneous. Your web app starts fast and stays fast.',
		image: 'fast-website.svg',
	},
	{
		title: 'Flexibility',
		description:
			'The web applications that we build can run in a mobile app, desktop application, or in a browser.',
		image: 'devices.svg',
	},
	{
		title: 'Improved performance',
		description:
			'Our websites are built with a more recently developed programming language that greatly improves speed, and efficiently.',
		image: 'page-meter.svg',
	},
];
