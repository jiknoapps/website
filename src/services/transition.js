import { cubicOut } from 'svelte/easing';

export function inlineFly(
	node,
	{ x = 0, y = 50, duration = 400, delay = 0 } = {}
) {
	node.style.position = 'relative';
	return {
		duration,
		delay,
		css: (t) => {
			const easing = cubicOut(t);
			const reverse = 1 - easing;

			return `
				opacity: ${easing};
				top: ${reverse * y}px;
				right: ${x * reverse}px;
			`;
		},
	};
}
