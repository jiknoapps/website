export const createApp = `I'm looking to build something.

Feel free to include any information you feel will make this process more effective, namely:
- Name
- Organisation
- Email
- Phone number
- Project description
- Website, mobile app, desktop app, other
- Approximate timeline
- Goals
`;

export const improveApp = `I'm looking to improve something.

Feel free to include any information you feel will make this process more effective, namely:
- Name
- Organisation
- Email
- Phone number
- Project description
- Approximate timeline
- Goals
`;
