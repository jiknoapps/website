export default [
	{
		value: 'Client-side routing',
		description:
			'A client-side route happens when the route is handled internally by the JavaScript that is loaded on the page and not code on the server.',
		jikno: true,
		wix: false,
		optuno: false,
		wordpress: false,
	},
	{
		value: 'All animations',
		description:
			'Any animations from simple 2d animations, to complex 3d animations, to scrolling animations, to whatever you can possibly think of!',
		jikno: true,
		wix: false,
		optuno: 'If you want to pay extra, Yes.',
		wordpress: false,
	},
	{
		value: 'Secure data storage',
		description: 'A data center that is safe, secure, and reliable.',
		jikno: true,
		wix: true,
		optuno: true,
		wordpress: true,
	},
	{
		value: 'Free custom email',
		description: 'A free custom domain email you@domain.com',
		jikno: true,
		wix: true,
		optuno: true,
		wordpress: true,
	},
	{
		value: 'Free SEO',
		description:
			'SEO (Search Engine Optimization) is the process of making a website more visible in search results, also termed improving search rankings.',
		jikno: true,
		wix: false,
		optuno: 'If you want to pay extra, Yes.',
	},
	{
		value: 'Free SSL',
		description:
			'Transport Layer Security (TLS), formerly known as Secure Sockets Layer (SSL), is a protocol used by applications to communicate securely across a network, preventing tampering with and eavesdropping on email, web browsing, messaging, and other protocols.',
		jikno: true,
		wix: true,
		optuno: true,
		wordpress: true,
	},
	{
		value: 'Online payments',
		description:
			'The ability to charge your customers through you website.',
		jikno: true,
		wix: true,
		optuno: true,
		wordpress: true,
	},
	{
		value: '100% Offline support',
		description:
			'Once your application loads the first time it is stable without a internet connection.',
		jikno: true,
		wix: false,
		optuno: false,
		wordpress: false,
	},
	{
		value: 'Premium customer support',
		description: 'Phone and Email customer support.',
		jikno: true,
		wix: true,
		optuno: true,
		wordpress: true,
	},
	{
		value: 'What ever you could possibly want',
		description: 'If it can be done, we will be the ones to get it done!',
		jikno: true,
		wix: false,
		optuno: false,
		wordpress: false,
	},
];
