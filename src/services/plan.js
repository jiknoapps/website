export default [
	{
		name: 'Simple',
		description:
			'Web apps that are more simple.  A blog would certainly work here, and so would a login, but probably not a app that revolves around a login.',
		price: '$215 for the first month then $15 / MO after',
	},
	{
		name: 'Complex Simple',
		description:
			'Web apps that are complex to build, but simple to maintain.<br /><br />In addition to everything in the simple plan, this includes:',
		features: ['2d and 3d animations', 'Advanced mathmatical calculations'],
		price: '$215 for the first month then $15 / MO after',
	},
	{
		name: 'Complex',
		description:
			'Web apps that are both complex to build, and complex to maintain. (Complex apps that center around storing information.)<br /><br />In addition to everything in the complex simple plan, this includes:',
		features: ['Storing and retreving information in great quantities', ''],
		price: '$215 for the first month then $15 / MO after',
	},
];
