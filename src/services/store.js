import { writable } from 'svelte/store';

export const onscroll = writable({});
export const scrollTo = writable([0]);
export const scrollPos = writable(0);
